## Ejercicio 3
* ¿Cuántos paquetes componen la captura?: 1268
* ¿Cuánto tiempo dura la captura?: 12.81
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes aproximadamente
* ¿Qué protocolos de nivel de red aparecen en la captura?: ethernet II
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4
* N : 50
* Dirección IP de la máquina A: 192.168.0.10
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete: 49154
* Puerto UDP hacia el que se envía el paquete: 54550
* SSRC del paquete: 0x2a173650
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): IYU-T G.711 PCMU
## Ejercicio 5
* ¿Cuál es el primer número de tu DNI / NIE?: 0
* ¿Cuántos paquetes hay en el flujo F?: 642
* ¿El origen del flujo F es la máquina A o B?: A
* ¿Cuál es el puerto UDP de destino del flujo F?: 49154
* ¿Cuántos segundos dura el flujo F?: 12.81
## Ejercicio 6
* ¿Cuál es la diferencia máxima en el flujo?: 12.837857
* ¿Cuál es el jitter medio del flujo?: 12.234262
* ¿Cuántos paquetes del flujo se han perdido?: 0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: todo el tiempo, el minimo está en 0.629375
## Ejercicio 7
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 92089
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: ha llegado dentro del rango valido
* ¿Qué jitter se ha calculado para ese paquete? 12.23ms
* ¿Qué timestamp tiene ese paquete? 4000
* ¿Por qué número hexadecimal empieza sus datos de audio?: 7
## Ejercicio 8
* Salida del comando date: mar 17 oct 2023 12:02:31 CEST
* Número total de paquetes en la captura: 7340
* Duración total de la captura (en segundos): 13.99
## Ejercicio 9
* ¿Cuántos paquetes RTP hay en el flujo?:1684
* ¿Cuál es el SSRC del flujo?: 0x042a3c08
* ¿En qué momento (en ms) de la traza comienza el flujo?: 3.964127681
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 28012
* ¿Cuál es el jitter medio del flujo?: 0
* ¿Cuántos paquetes del flujo se han perdido?: 0